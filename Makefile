help: ## Show help
	# From https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n",$$1,$$2}'


# Check

check-lint: ## CHECK - Runs black and ruff
	black . --check --diff
	ruff check .


# DEPLOY

deploy-migrate: ## Run manage migrate
	python manage.py makemigrations
	python manage.py migrate

deploy-update: ## DEPLOY - Runs poetry update
	poetry update

deploy-requirements: deploy-update ## DEPLOY - Extracts requirements from poetry 
	poetry export --without dev --without-hashes --format=requirements.txt > requirements.txt

deploy: deploy-requirements deploy-migrate ## DEPLOY - Prepares for deploy
	ruff check .
	black .


# TEST

test-super-user: ## TEST - Creates generic admin superuser
	poetry run python manage.py createsuperuser --username admin --email admin@example.com

test-unittest: ## TEST - Runs test suite
	poetry run python manage.py test -v 3

test: ## TEST - Runs server on 8000
	poetry run python manage.py runserver
