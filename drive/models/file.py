from django.contrib.auth.models import User
from django.db import models

from drive.models.folder import Folder


class File(models.Model):
    file = models.FileField(
        blank=False,
        help_text="Select a file to upload",
        null=False,
        unique=False,
        upload_to="files/",
        verbose_name="File Name",
    )
    parent = models.ForeignKey(
        Folder,
        blank=True,
        help_text="Select the parent directory",
        null=True,
        on_delete=models.CASCADE,
        related_name="file_child",
        verbose_name="Parent Directory",
    )
    owner = models.ForeignKey(
        User,
        blank=False,
        help_text="Select the owner of the file",
        null=False,
        on_delete=models.CASCADE,
        related_name="file_owner",
        verbose_name="Owner",
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def name(self):
        return self.file.name.split("/")[-1]
