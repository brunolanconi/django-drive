from django.contrib import admin

from drive.models.file import File
from drive.models.folder import Folder


class FileAdmin(admin.ModelAdmin):
    search_fields = (
        "file",
        "parent",
        "owner",
    )
    list_display = (
        "file",
        "parent",
        "owner",
        "created",
        "updated",
    )
    list_display_links = ("file",)


admin.site.register(File, FileAdmin)


class FolderAdmin(admin.ModelAdmin):
    search_fields = (
        "name",
        "parent",
        "owner",
    )
    list_display = (
        "name",
        "parent",
        "owner",
        "created",
        "updated",
    )
    list_display_links = ("name",)


admin.site.register(Folder, FolderAdmin)
