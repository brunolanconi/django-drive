from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User
from django.shortcuts import redirect, render
from django.views import View

from setup.utils import add_error, get_errors


class LoginView(View):
    def get(self, request):
        errors = get_errors(
            request=request,
            url_name="login",
        )

        return render(request, "authentication/login.html", {"errors": errors})

    def post(self, request):
        data = request.POST
        username = data.get("username")
        password = data.get("password")

        if not username or not password:
            add_error(
                message="Please fill in all the fields",
                request=request,
                tag="login_error",
                url_name="login",
            )
            return redirect("login")

        user = authenticate(request, username=username, password=password)

        if not user:
            add_error(
                message="Invalid username or password",
                request=request,
                tag="login_error",
                url_name="login",
            )
            return redirect("login")

        login(request, user)
        return redirect("home")


class SignupView(View):
    def get(self, request):
        errors = get_errors(
            request=request,
            url_name="signup",
        )

        return render(request, "authentication/signup.html", {"errors": errors})

    def post(self, request):
        data = request.POST
        username = data.get("username")
        password = data.get("password")

        if not username or not password:
            add_error(
                message="Please fill in all the fields",
                request=request,
                tag="signup_error",
                url_name="signup",
            )
            return redirect("signup")

        user = User.objects.filter(username=username).first()

        if user:
            add_error(
                message="User already exists",
                request=request,
                tag="signup_error",
                url_name="signup",
            )
            return redirect("signup")

        # LINK: https://docs.djangoproject.com/en/5.1/topics/auth/default/#creating-users
        user = User.objects.create_user(username, username, password)
        user.save()

        return redirect("login")


class LogoutView(LoginRequiredMixin, View):
    def get(self, request):
        return render(request, "authentication/logout.html")

    def post(self, request):
        logout(request)
        return redirect("login")
