from django.urls import path

from authentication.views import LoginView, LogoutView, SignupView

urlpatterns = [
    # path("", AccountView.as_view(), name="account"),
    path("login", LoginView.as_view(), name="login"),
    path("signup", SignupView.as_view(), name="signup"),
    path("logout", LogoutView.as_view(), name="logout"),
]
