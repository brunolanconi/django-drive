# Django Drive

## Dependencies ##

- [Poetry](https://python-poetry.org/docs/#installing-with-pipx)

## Getting Started ##

### Development ###

1. Install Dependencies and Make Migrations

```bash
make deploy
```

2. Run Server

```bash
make test
```

#### Tips ####

When adding new apps, make sure to add them to the `INSTALLED_APPS` in `settings.py` and to the `pyproject.toml`.

```bash
python manage.py startapp appname
```

```toml
...
packages = [
    { include = "./appname" },
]
...
```

## Commands ##

```bash
check-lint                     CHECK - Runs black and ruff
deploy-migrate                 Run manage migrate
deploy-requirements            DEPLOY - Extracts requirements from poetry 
deploy-update                  DEPLOY - Runs poetry update
deploy                         DEPLOY - Prepares for deploy
help                           Show help
test-super-user                TEST - Creates generic admin superuser
test-unittest                  TEST - Runs test suite
test                           TEST - Runs server on 8000
```

## Useful links ## 

- https://devcenter.heroku.com/articles/config-vars
- https://devcenter.heroku.com/articles/django-app-configuration
- https://devpress.csdn.net/cicd/62ec0fab19c509286f416271.html
- https://django-storages.readthedocs.io/en/latest/backends/amazon-S3.html
- https://docs.djangoproject.com/en/1.11/topics/auth/customizing/#extending-the-existing-user-model
- https://docs.gitlab.com/ee/ci/cloud_deployment/heroku.html
- https://gitlab.com/brunolanconi/rea-backend/-/settings/ci_cd
- https://hackernoon.com/managing-django-media-and-static-files-on-heroku-with-bucketeer
- https://medium.com/@ganiilhamirsyadi/how-to-deploy-django-app-on-heroku-using-gitlab-ci-cd-step-by-step-45c0d9d31798
- https://simpleisbetterthancomplex.com/2015/11/26/package-of-the-week-python-decouple.html
- https://simpleisbetterthancomplex.com/tutorial/2018/11/22/how-to-implement-token-authentication-using-django-rest-framework.html

## Troubleshoot ##

- Auto import in VSCode vs rest_framework

```json
...
"python.analysis.autoImportCompletions": true,
"python.analysis.indexing": true,
"python.analysis.packageIndexDepths": [
    {
        "name": "django",
        "depth": 10,
        "includeAllSymbols": true,
    },
    {
        "name": "rest_framework",
        "depth": 10,
        "includeAllSymbols": true,
    }
]
...
```
