def add_error(
    message: str,
    request,
    tag: str,
    url_name: str,
) -> None:
    """
    Adds an error message to the session for a specific URL.

    Args:
        message (str): The error message to be added.
        request: The request object.
        tag (str): The tag to identify the error.
        url_name (str): The name of the URL.
    """
    error_tag = f"error_{url_name}_{tag}"
    request.session[error_tag] = message


def get_errors(request, url_name: str) -> dict:
    """
    Retrieve and remove errors from the session based on the given URL name.

    Args:
        request: The current request object.
        url_name: The name of the URL.

    Returns:
        A dictionary containing the retrieved errors, where the keys are the error tags and the values are the error messages.
    """
    error_prefix_tag = f"error_{url_name}_"
    current_errors = {}

    for key in list(request.session.keys()):
        if error_prefix_tag in key:
            tag = key.replace(error_prefix_tag, "")
            current_errors[tag] = request.session.pop(key)

    return current_errors
